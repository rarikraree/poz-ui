import React from "react";

import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import AppsIcon from "mdi-material-ui/Apps";

const useClasses = makeStyles((theme) =>
  createStyles({
    appbar: {
      boxShadow: "none",
    },
    appsButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      userSelect: "none",
    },
  }),
);

export interface Props {
  title: string;
}

const AppToolbar: React.FC<Props> = ({ title }) => {
  const classes = useClasses();

  return (
    <AppBar
      className={classes.appbar}
      position="sticky"
    >
      <Toolbar variant="dense">
        <IconButton
          className={classes.appsButton}
          aria-label="apps"
          color="inherit"
          edge="start"
        >
          <AppsIcon />
        </IconButton>
        <Typography
          className={classes.title}
          color="inherit"
          variant="h6"
        >
          {title}
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default AppToolbar;
