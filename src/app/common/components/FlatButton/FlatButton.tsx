import React from "react";
import clsx from "clsx";

import Button from "@material-ui/core/Button";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useClasses = makeStyles(() =>
  createStyles({
    button: {
      boxShadow: "none",
    },
  }),
);

interface Props {
  className?: string;
  [key: string]: any;
}

const FlatButton: React.FC<Props> = ({ children, className, ...props }) => {
  const classes = useClasses();

  return (
    <Button
      className={clsx(classes.button, className)}
      {...props}
    >
      {children}
    </Button>
  );
};

export default FlatButton;
