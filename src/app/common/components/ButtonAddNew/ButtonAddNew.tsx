import React from "react";
import clsx from "clsx";

import Button from "@material-ui/core/Button";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import PlusIcon from "mdi-material-ui/Plus";

const useClasses = makeStyles((theme) =>
  createStyles({
    root: {
      color: "inherit",
      minWidth: 24,
      boxShadow: "none",
    },
  }),
);

export interface Props {
  cls?: {
    wrapper?: string;
    button?: string;
  };
}

const ButtonAddNew: React.FC<Props> = ({ cls = {} }) => {
  const classes = useClasses();

  return (
    <div
      className={cls.wrapper}
    >
      <Button
        classes={{
          root: clsx(classes.root, cls.button),
        }}
        color="secondary"
        size="small"
        variant="contained"
      >
        <PlusIcon />
      </Button>
    </div>
  );
};

export default ButtonAddNew;
