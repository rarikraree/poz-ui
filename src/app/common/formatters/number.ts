export const number = (value: number): string =>
  value.toString().replace(/\d(?=(\d{3})+\.)/g, "$&,");
