export const currency = (value: number, fractionDigits: number = 2): string =>
  value.toFixed(fractionDigits).replace(/\d(?=(\d{3})+\.)/g, "$&,");
