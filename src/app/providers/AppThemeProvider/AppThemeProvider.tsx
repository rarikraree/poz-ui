import React from "react";

import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";

import CssBaseline from "@material-ui/core/CssBaseline";
import blue from "@material-ui/core/colors/blue";
import blueGrey from "@material-ui/core/colors/blueGrey";

import "normalize.css";
import "typeface-roboto";
import "./app.css"

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: blueGrey,
  },
  typography: {
    allVariants: {
      color: "#333",
    }
  },
});

const AppThemeProvider: React.FC = ({ children }) => (
  <React.Fragment>
    <CssBaseline />
    <ThemeProvider theme={theme}>
      {children}
    </ThemeProvider>
  </React.Fragment>
);

export default AppThemeProvider;
