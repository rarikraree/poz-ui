import React from "react";
import { Provider } from "react-redux";

import AppStore from "../../redux/AppStore";

const AppStoreProvider: React.FC = ({ children }) => (
  <Provider store={AppStore}>
    {children}
  </Provider>
);

export default AppStoreProvider;
