import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import PoZ from "../../modules/PoZ";

const AppRoutesProvider: React.FC = () => (
  <Router>
    <Route path="/poz/:page" component={PoZ} />
  </Router>
);


export default AppRoutesProvider;
