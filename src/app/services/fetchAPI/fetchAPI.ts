import { Settings } from "./Settings";

export interface IFetchOptions extends RequestInit {
  path: string;
}

const resolveURL = (path: string) => `${Settings.host}${path}`;

const defaultHeaders = {
  "Accept": "*/*",
  "Content-Type": "application/json",
};

export const fetchAPI = async (options: IFetchOptions) => {
  const { path, headers, ...reqInit } = options;

  return fetch(
    resolveURL(path),
    {
      headers: headers || defaultHeaders,
      ...reqInit,
    },
  )
    .then((res) => res.json());
};
