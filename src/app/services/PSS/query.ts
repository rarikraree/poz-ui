import { fetchAPI } from "../fetchAPI";

interface IDataQuery {
  query: string;
}

export const query = (data: IDataQuery) => {
  return fetchAPI({
    method: "post",
    path: "/pss/query",
    body: JSON.stringify(data),
  });
};
