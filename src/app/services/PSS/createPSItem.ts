import { fetchAPI } from "../fetchAPI";

interface IDataCommand {
  id: string;
  sku: string;
  type: string;
  title: string;
  price: number;
  barcode?: string;
}

export const createPSItem = (data: IDataCommand) => {
  return fetchAPI({
    method: "post",
    path: "/pss/item",
    body: JSON.stringify(data),
  });
};
