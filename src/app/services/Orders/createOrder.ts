import { fetchAPI } from "../fetchAPI";

interface IDataCommand {
  id: string;
  ref: string;
  itemCount: number;
  orderTotal: number;
  status: string;
}

export const createOrder = (data: IDataCommand) => {
  return fetchAPI({
    method: "post",
    path: "/poz/orders",
    body: JSON.stringify(data),
  });
};
