import React, { Suspense } from "react";

import AppRoutesProvider from "./providers/AppRoutesProvider";
import AppStoreProvider from "./providers/AppStoreProvider";
import AppThemeProvider from "./providers/AppThemeProvider";

const Loader: React.FC = () => (
  <div>loading...</div>
);

const App: React.FC = () => (
  <Suspense fallback={<Loader />}>
    <AppStoreProvider>
      <AppThemeProvider>
        <AppRoutesProvider />
      </AppThemeProvider>
    </AppStoreProvider>
  </Suspense>
);

export default App;
