import React from "react";
import { useDispatch } from "react-redux";
import { Command as Modularize } from "./ducks/Actions/Modularize";
import { Sessions } from "./pages";

interface Props {
  match: {
    path: string;
    url: string;
    isExact: boolean;
  };
}

const Modular: React.FC<Props> = () => {
  const dispatch = useDispatch();
  dispatch(Modularize());

  return (
    <Sessions />
  );
};

export default Modular;
