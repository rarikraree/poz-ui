import React from "react";
import clsx from "clsx";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useClasses = makeStyles(() =>
  createStyles({
    column: {
      color: "#333",
      display: "flex",
      fontSize: "1.04rem",
      fontWeight: 500,
      padding: "0 8px",
    },
  }),
);

interface Props {
  className?: string;
}

const Column: React.FC<Props> = ({ className, children }) => {
  const classes = useClasses();

  return (
    <div className={clsx(classes.column, className)}>{children}</div>
  );
};

export default Column;
