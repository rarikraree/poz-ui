import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { useTranslation } from "react-i18next";

const useClasses = makeStyles(() =>
  createStyles({
    line: {
      borderBottom: "1px solid #ddd",
      color: "#555",
      cursor: "pointer",
      display: "block",
      padding: 8,
      textAlign: "center",
      userSelect: "none",
      width: "100%",
    },
  }),
);

const EmptyList: React.FC = () => {
  const classes = useClasses();
  const { t } = useTranslation("poz");

  return (
    <div className={classes.line}>
      {t("orderline.noRecords")}
    </div>
  );
};

export default EmptyList;
