import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { IOrderline } from "../../../../../../../ducks/States/IOrderline";

const useClasses = makeStyles(() =>
  createStyles({
    wrapper: {
    },
    input: {
      fontSize: "0.95rem",
      margin: "0 2px",
      width: 42,
    },
  }),
);

interface Props {
  line: IOrderline;
  setChanged: (props: { item: IOrderline; src: string; }) => void;
}

const removeDiscount = (orderline: any, setChanged: any) => {
  const item = Object.assign({}, orderline, { discount: undefined });
  setChanged({ item, src: "Discount" });
};

const ActiveDiscount: React.FC<Props> = ({ line, setChanged }) => {
  const classes = useClasses();
  const value = line.discount ? line.discount.value : "";
  const handleChange = (value: string) => {
    value = value.trim();

    if (value === "" || value === "-") {
      removeDiscount(line, setChanged);
    } else {
      const isPercent = value.indexOf("%") > -1 ? true : false;
      let reformed = "";

      if (isPercent) {
        reformed = value.replace("%", "");
      } else {
        reformed = value;
      }

      let number = Number(reformed);

      if (!Number.isNaN(number)) {
        if (number < 0) {
          removeDiscount(line, setChanged);
        } else {
          let item: any;

          if (isPercent) {
            number = number > 100 ? 100 : number;
            const amount = (line.price * number) / 100;
            const percent = number;
            const discount = {
              amount,
              percent,
              value,
            };

            item = Object.assign({}, line, { discount });
          } else {
            const amount = number;
            const percent = (number * 100) / line.price;
            const discount = {
              amount,
              percent,
              value,
            };

            item = Object.assign({}, line, { discount });
          }

          setChanged({ item, src: "Discount" });
        }
      }
    }
  };

  return (
    <div className={classes.wrapper}>
      <input
        className={classes.input}
        onChange={(e) => handleChange(e.target.value)}
        value={value}
      />
    </div>
  );
};

export default ActiveDiscount;
