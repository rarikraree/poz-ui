import React from "react";
import { useSelector } from "react-redux";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import OrderBar from "./OrderBar";
import OrderInput from "./OrderInput";
import OrderHeader from "./OrderHeader";
import OrderList from "./OrderList";
import OrderTotal from "./OrderTotal";

import { IPoZState } from "../../../../../ducks/IPoZState";

const useClasses = makeStyles(() =>
  createStyles({
    order: {
      display: "flex",
      flex: 1,
      flexDirection: "column",
      height: "100%",
      overflow: "auto",
      padding: 0,
    },
    ordertopper: {
      backgroundColor: "#fff",
      padding: "8px 8px 1px",
      position: "sticky",
      top: 0,
    },
    orderlist: {
      backgroundColor: "#fff",
      flex: 1,
      padding: "0 8px",
    },
    orderbotter: {
      backgroundColor: "#fff",
      bottom: 0,
      padding: "1px 8px 8px",
      position: "sticky",
    },
  }),
);

interface Props {
}

const Order: React.FC<Props> = () => {
  const classes = useClasses();
  const ActiveOrder = useSelector((state: { PoZ: IPoZState; }) => state.PoZ.ActiveOrder);

  return (
    <React.Fragment>
      {ActiveOrder &&
        <div className={classes.order}>
          <OrderBar orderID={ActiveOrder._id} />
          <div className={classes.ordertopper}>
            <OrderInput />
            <OrderHeader />
          </div>
          <div className={classes.orderlist}>
            <OrderList />
          </div>
          <div className={classes.orderbotter}>
            <OrderTotal />
          </div>
        </div>
      }
    </React.Fragment>
  );
};

export default Order;
