import React from "react";

import { useTranslation } from "react-i18next";

import Form from "./Form";
import { IForm } from "./Form/IForm";
import { IFieldData } from "./Form/IFormField";

interface Props {
  initialValue: {
    _id: string;
    type: string;
    sku: string;
    title: string;
    price: string;
    barcode: string;
    vendor: string;
  };
  onFieldChange?: (fieldChange: { field: IFieldData; key: string; value: any; }) => void;
}

const CreatePSItemForm: React.FC<Props> = ({ initialValue, onFieldChange }) => {
  const { t } = useTranslation("poz");
  const form: IForm = {
    type: {
      type: "select",
      label: t("field.type"),
      isRequired: true,
      select: [
        { key: "product", value: t("field.types.product") },
        { key: "service", value: t("field.types.service") },
      ],
      value: initialValue.type || "product",
      placeholder: "Type",
    },
    sku: {
      type: "text",
      label: t("field.sku"),
      isRequired: true,
      value: initialValue.sku || "",
      placeholder: "SKU",
    },
    title: {
      type: "text",
      label: t("field.title"),
      isRequired: true,
      value: initialValue.title || "",
      placeholder: "Title",
    },
    price: {
      type: "number",
      label: t("field.retailPrice"),
      isRequired: true,
      value: initialValue.price || "1.00",
      placeholder: "Retail Price",
    },
    barcode: {
      type: "text",
      label: t("field.barcode"),
      value: initialValue.barcode || "",
      placeholder: "Barcode",
    },
    vendor: {
      type: "text",
      label: t("field.vendor"),
      isRequired: true,
      value: initialValue.vendor || "",
      placeholder: "Vendor | BU",
    },
  };

  return (
    <Form
      form={form}
      onFieldChange={onFieldChange}
    />
  );
};

export default CreatePSItemForm;
