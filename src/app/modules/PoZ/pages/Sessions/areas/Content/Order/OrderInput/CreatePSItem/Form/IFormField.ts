export interface IFormField {
  label: string;
  type: "text" | "number" | "select";
  isRequired?: boolean;
  value?: string | number;
  placeholder?: string;
  select?: { key: string; value: string; }[];
}

export interface IFieldData extends IFormField {
  key: string;
  isRequired: boolean;
  value: string | number;
}
