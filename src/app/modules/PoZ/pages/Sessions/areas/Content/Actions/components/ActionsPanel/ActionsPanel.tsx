import React from "react";

import Typography from "@material-ui/core/Typography";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useClasses = makeStyles((theme) =>
  createStyles({
    actionspanel: {
      backgroundColor: "#fff",
      borderColor: "#ddd",
      borderStyle: "solid",
      borderWidth: "0 0 1px",
      display: "flex",
      flexDirection: "column",
      padding: "12px 16px",
    },
    row: {
      display: "flex",
      flex: 1,
      marginBottom: 8,
    },
    title: {
      color: "#27d",
      flex: 1,
      fontSize: "1.1rem",
      fontWeight: 600,
      textTransform: "uppercase",
    },
  }),
);

interface Props {
  title: string;
  TitleSide?: any;
  TitleSideProps?: any;
}

const ActionsPanel: React.FC<Props> = ({ children, TitleSide, TitleSideProps, title }) => {
  const classes = useClasses();

  return (
    <div className={classes.actionspanel}>
      <div className={classes.row}>
        <Typography className={classes.title}>{title}</Typography>
        {TitleSide && <TitleSide {...TitleSideProps} />}
      </div>
      {children}
    </div>
  );
};

export default ActionsPanel;
