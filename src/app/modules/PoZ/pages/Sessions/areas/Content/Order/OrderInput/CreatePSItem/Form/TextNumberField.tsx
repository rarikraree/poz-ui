import React from "react";

import TextField from "@material-ui/core/TextField";

import { IFieldData } from "./IFormField";

interface Props {
  field: IFieldData;
  onFieldChange?: (fieldChange: { field: IFieldData; key: string; value: any; }) => void;
}

const TextNumberField: React.FC<Props> = ({ field, onFieldChange, ...rest }) => (
  <TextField
    label={field.label}
    required={field.isRequired}
    placeholder={field.placeholder}
    type={field.type}
    value={field.value}
    fullWidth={true}
    onChange={onFieldChange ? (e) => onFieldChange({ field, key: field.key, value: e.target.value }) : undefined}
    InputLabelProps={{
      shrink: true,
    }}
    {...rest}
  />
);

export default TextNumberField;
