import React from "react";
import { useSelector } from "react-redux";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useClasses = makeStyles(() =>
  createStyles({
    wrapper: {
      display: "flex",
      flex: 1,
      flexDirection: "column",
    },
    customer: {
    },
  }),
);

interface Props {
  state: any;
  setState: any;
}

const Customer: React.FC<Props> = ({ setState, state }) => {
  const classes = useClasses();

  return (
    <div className={classes.wrapper}>
      <TextField
        className={classes.customer}
        label="Drop n Go #"
        InputLabelProps={{
          shrink: true,
        }}
        placeholder="Drop n Go #"
        onChange={(e) => {
          const ref = e.target.value;
          const newState = {
            ...state,
            customer: {
              ...state.customer,
              ref,
            },
          };
          setState(newState);
        }}
        value={state.customer.ref}
      />
    </div>
  );
};

export default Customer;
