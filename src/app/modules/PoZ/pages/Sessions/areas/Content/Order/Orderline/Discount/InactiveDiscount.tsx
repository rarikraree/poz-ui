import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { currency } from "../../../../../../../../../common/formatters/currency";
import { IOrderline } from "../../../../../../../ducks/States/IOrderline";

const useClasses = makeStyles(() =>
  createStyles({
    discount: {
      display: "block",
    },
    percent: {
      color: "#666",
      display: "block",
      fontSize: "0.9rem",
    },
  }),
);

interface Props {
  line: IOrderline;
}

const InactiveDiscount: React.FC<Props> = ({ line }) => {
  const classes = useClasses();

  const discount = line.discount ? currency(line.discount.amount) : "-";
  const percent = line.discount ? `${line.discount.percent}%` : "";

  return (
    <React.Fragment>
      <div className={classes.discount}>
        {discount}
      </div>
      <div className={classes.percent}>
        {percent}
      </div>
    </React.Fragment>
  );
};

export default InactiveDiscount;
