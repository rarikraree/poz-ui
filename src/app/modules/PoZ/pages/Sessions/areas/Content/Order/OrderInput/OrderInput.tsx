import React, { useState } from "react";
import { useDispatch } from "react-redux";

import IconButton from "@material-ui/core/IconButton";
import InputBase from "@material-ui/core/InputBase";
import Typography from "@material-ui/core/Typography";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import MagnifyIcon from "mdi-material-ui/Magnify";

import { useTranslation } from "react-i18next";

import CreatePSItem from "./CreatePSItem";
import { Command as OrderLineQuerying } from "../../../../../../ducks/SagaActions/OrderlineQuerying";

const useClasses = makeStyles(() =>
  createStyles({
    orderbar: {
      backgroundColor: "#468",
      borderRadius: "8px 8px 0 0",
      color: "#fff",
      display: "flex",
      padding: "0 8px",
    },
    text: {
      color: "inherit",
      fontWeight: "bold",
      margin: "0 16px 0 24px",
      padding: "12px 0 0",
      userSelect: "none",
    },
    orderinput: {
      backgroundColor: "#fff",
      borderRadius: 4,
      display: "flex",
      flex: 1,
      margin: 8,
      padding: "0 8px",
    },
    orderactions: {
      maxWidth: 400,
      minWidth: 160,
      padding: "6px 8px",
      textAlign: "right",
      width: "26.5%",
    },
  }),
);

const useInputBase = makeStyles(() =>
  createStyles({
    root: {
      flex: 1,
      paddingRight: 4,
    },
  }),
);

export interface Props {
}

const OrderBar: React.FC<Props> = () => {
  const classes = useClasses();
  const inputBase = useInputBase();

  const { t } = useTranslation("poz");
  const [value, setValue] = useState("");
  const [firstKey, setFirstKey] = useState<any>(undefined);
  const dispatch = useDispatch();
  const getInputTimeRange = () => {
    const now = new Date();
    return now.getTime() - firstKey;
  };

  const handleKeyDown = (e: React.KeyboardEvent) => {
    if (e.keyCode === 13) {
      handleOrderLineQuery(value);
    }
  };

  const handleChange = (v: string) => {
    if (v.length === 0) {
      setFirstKey(undefined);
    } else if (v.length > 0 && firstKey === undefined) {
      setFirstKey((new Date()).getTime());
    }

    setValue(v);
  }

  const handleOrderLineQuery = (query: string) => {
    if (query.length > 0) {
      dispatch(OrderLineQuerying({ query }));
    }

    setFirstKey(undefined);
    setValue("");
  };

  return (
    <div className={classes.orderbar}>
      <Typography
        className={classes.text}
        variant="button"
      >
        {t("orderline.input")}
      </Typography>
      <div className={classes.orderinput}>
        <InputBase
          classes={inputBase}
          placeholder={"SKU / Barcode Product"}
          onChange={(e) => handleChange(e.target.value)}
          onKeyDown={handleKeyDown}
          value={value}
        />
        <IconButton
          aria-label="search"
          size="small"
          onClick={() => handleOrderLineQuery(value)}
        >
          <MagnifyIcon />
        </IconButton>
      </div>
      <div className={classes.orderactions}>
        <CreatePSItem />
      </div>
    </div>
  );
};

export default OrderBar;
