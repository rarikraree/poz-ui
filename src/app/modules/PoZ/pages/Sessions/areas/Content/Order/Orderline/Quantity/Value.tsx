import React from "react";

import { IOrderline } from "../../../../../../../ducks/States/IOrderline";

import ActiveValue from "./ActiveValue";
import InactiveValue from "./InactiveValue";

interface Props {
  isActive: boolean;
  line: IOrderline;
  removeOrderline: () => void;
  setChanged: (props: { item: IOrderline; src: string; }) => void;
}

const Value: React.FC<Props> = ({ isActive, line, removeOrderline, setChanged }) => (
  <React.Fragment>
    {isActive ?
      (
        <ActiveValue
          line={line}
          removeOrderline={removeOrderline}
          setChanged={setChanged}
        />
      ) : (
        <InactiveValue
          line={line}
        />
      )}
  </React.Fragment>
);

export default Value;
