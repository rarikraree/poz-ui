import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { useTranslation } from "react-i18next";

import { currency } from "../../../../../../../../../common/formatters/currency";
import { IOrderline } from "../../../../../../../ducks/States/IOrderline";
import Column from "../Column";

import { resolvePrice } from "../Price";

const useClasses = makeStyles(() =>
  createStyles({
    column: {
      flexDirection: "column",
      minWidth: "136px",
      textAlign: "right",
    },
    total: {
      display: "block",
    },
    actions: {
      fontSize: "0.9rem",
      textDecoration: "underline",
      textTransform: "uppercase",
    },
    discard: {
      marginRight: 6,
    },
    confirm: {
    },
  }),
);

interface Props {
  isActive: boolean;
  line: IOrderline;
  onConfirm?: () => void;
  onDiscard?: () => void;
}

export const resolveTotal = (orderline: any) => {
  const { sale } = resolvePrice(orderline);
  const quantity = orderline.quantity || 1;

  return { total: sale * quantity };
};

const Total: React.FC<Props> = ({ isActive, line, onConfirm, onDiscard }) => {
  const classes = useClasses();
  const { t } = useTranslation("poz");
  const { total } = resolveTotal(line);
  const t_total = currency(total);

  return (
    <Column className={classes.column}>
      <div className={classes.total}>
        {t_total}
      </div>
      {isActive &&
        <div className={classes.actions}>
          <span
            className={classes.discard}
            onClick={onDiscard}
          >
            {t("common.discard")}
          </span>
          <span
            className={classes.confirm}
            onClick={onConfirm}
          >
            {t("common.confirm")}
          </span>
        </div>
      }
    </Column>
  );
};

export default Total;
