import React from "react";

import Typography from "@material-ui/core/Typography";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import OrderMenu from "./OrderMenu";

const useClasses = makeStyles(() =>
  createStyles({
    orderbar: {
      backgroundColor: "#fff",
      display: "flex",
      padding: "4px 8px 0 16px",
    },
    title: {
      color: "#346",
      display: "inline-block",
      fontSize: "1.1rem",
      fontWeight: 600,
      padding: "4px 8px 0 0",
      textTransform: "uppercase",
      userSelect: "none",
    },
    id: {
      color: "#346",
      display: "inline-block",
      fontSize: "0.9rem",
      fontWeight: 500,
      padding: "7px 0 0",
      textTransform: "uppercase",
      userSelect: "none",
    },
    idtext: {
      userSelect: "text",
    },
    text: {
      flex: 1,
    },
  }),
);

export interface Props {
  orderID: string;
}

const OrderBar: React.FC<Props> = ({ orderID }) => {
  const classes = useClasses();

  return (
    <div className={classes.orderbar}>
      <div className={classes.text}>
        <Typography className={classes.title}>ORDER</Typography>
        <Typography className={classes.id}>
          [<span className={classes.idtext}>{orderID}</span>]
        </Typography>
      </div>
      <OrderMenu />
    </div>
  );
};

export default OrderBar;
