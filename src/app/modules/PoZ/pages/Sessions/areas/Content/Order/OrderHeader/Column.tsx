import React from "react";
import clsx from "clsx";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useClasses = makeStyles(() =>
  createStyles({
    column: {
      fontWeight: "bold",
      padding: "0 8px",
      userSelect: "none",
    },
  }),
);

export interface Props {
  className?: string;
}

const Column: React.FC<Props> = ({ className, children }) => {
  const classes = useClasses();

  return (
    <span className={clsx(classes.column, className)}>{children}</span>
  );
};

export default Column;
