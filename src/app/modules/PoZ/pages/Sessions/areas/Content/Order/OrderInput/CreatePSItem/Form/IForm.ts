import { IFormField } from "./IFormField";

export interface IForm {
  [field: string]: IFormField;
}
