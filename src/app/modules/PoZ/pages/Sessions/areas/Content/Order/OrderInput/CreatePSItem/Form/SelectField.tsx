import React from "react";

import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

import { IFieldData } from "./IFormField";

interface Props {
  field: IFieldData;
  onFieldChange?: (fieldChange: { field: IFieldData; key: string; value: any; }) => void;
}

const SelectField: React.FC<Props> = ({ field, onFieldChange, ...rest }) => (
  <FormControl fullWidth={true}>
    <InputLabel>{field.label}</InputLabel>
    <Select
      required={field.isRequired}
      placeholder={field.placeholder}
      value={field.value}
      fullWidth={true}
      onChange={onFieldChange ? (e) => onFieldChange({ field, key: field.key, value: e.target.value }) : undefined}
      {...rest}
    >
      {field.select!.map((each) => (
        <MenuItem key={each.key} value={each.key}>{each.value}</MenuItem>
      ))}
    </Select>
  </FormControl>
);

export default SelectField;
