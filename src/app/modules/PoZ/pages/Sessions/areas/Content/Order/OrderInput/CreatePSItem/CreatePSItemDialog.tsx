import React from "react";
import { connect, useDispatch } from "react-redux";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { useTranslation } from "react-i18next";

import CreatePSItemForm from "./CreatePSItemForm";
import { Command as PSItemCreating } from "../../../../../../../ducks/SagaActions/PSItemCreating";

const useClasses = makeStyles(() =>
  createStyles({
    formWrapper: {
      display: "flex",
      flex: 1,
      padding: "0 200px 0 32px",
    },
    cancel: {
      fontSize: "0.9rem",
      minWidth: 80,
    },
    confirm: {
      boxShadow: "none",
      fontSize: "0.9rem",
      minWidth: 80,
    },
  }),
);

interface Props {
  initialValue: {
    _id: string;
    type: string;
    sku: string;
    title: string;
    price: string;
    barcode: string;
    vendor: string;
  };
  isOpen: boolean;
  toggle: (isOpen: boolean) => void;
}

const CreatePSItemDialog: React.FC<Props> = ({ initialValue, isOpen, toggle }) => {
  const classes = useClasses();
  const { t } = useTranslation("poz");
  const [values, setValues] = React.useState(initialValue);
  const dispatch = useDispatch();

  const onFieldChange = (fieldChange: any) => {
    const { key, value } = fieldChange;
    setValues(Object.assign({}, values, {
      [key]: value,
    }));
  };

  const handleSubmit = (e: any) => {
    const { barcode, price, sku, title, type, vendor, _id } = values;

    dispatch(PSItemCreating({
      barcode,
      id: _id,
      price: Number(price),
      sku,
      title,
      type,
      vendor,
    }));

    e.preventDefault();
  };

  return (
    <Dialog
      open={isOpen}
      fullWidth={true}
      maxWidth="md"
    >
      <form
        onSubmit={handleSubmit}
      >
        <DialogTitle>{t("sessions.CreatePSItem")}</DialogTitle>
        <DialogContent dividers>
          <div className={classes.formWrapper}>
            <CreatePSItemForm
              initialValue={values}
              onFieldChange={onFieldChange}
            />
          </div>
        </DialogContent>
        <DialogActions>
          <Button
            className={classes.cancel}
            onClick={() => toggle(false)}
            variant="outlined"
          >
            {t("common.discard")}
          </Button>
          <Button
            className={classes.confirm}
            color="primary"
            type="submit"
            variant="contained"
          >
            {t("common.confirm")}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

const mapStateToProps = (state: any) => ({
  initialValue: state.PoZ.NewItem,
});

const mapDispatchToProps = (dispatch: any) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(CreatePSItemDialog);
