import React from "react";

import IconButton from "@material-ui/core/IconButton";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import MinusIcon from "mdi-material-ui/Minus";
import PlusIcon from "mdi-material-ui/Plus";
import DeleteIcon from "mdi-material-ui/Delete";

import { IOrderline } from "../../../../../../../ducks/States/IOrderline";

const useClasses = makeStyles(() =>
  createStyles({
    quantity: {
      flex: 1,
      textAlign: "right",
    },
    delete: {
    },
    input: {
      fontSize: "0.95rem",
      margin: "0 2px",
      width: 42,
    },
    iconText: {
      fontSize: 18,
    },
  }),
);

interface Props {
  line: IOrderline;
  removeOrderline: () => void;
  setChanged: (props: { item: IOrderline; src: string; }) => void;
}

const ActiveValue: React.FC<Props> = ({ line, removeOrderline, setChanged }) => {
  const classes = useClasses();
  const value = line.quantity || 1;
  const handleChange = (quantity: number) => {
    if (!Number.isNaN(quantity)) {
      if (quantity < 1) {
        quantity = 1;
      }

      const item = Object.assign({}, line, { quantity });

      setChanged({ item, src: "Quantity" });
    }
  };

  return (
    <div className={classes.quantity}>
      <IconButton
        size="small"
        onClick={removeOrderline}
      >
        <DeleteIcon fontSize="inherit" />
      </IconButton>
      <IconButton
        size="small"
        onClick={() => handleChange(value - 1)}
      >
        <MinusIcon fontSize="inherit" />
      </IconButton>
      <input
        className={classes.input}
        onChange={(e) => handleChange(Number(e.target.value))}
        value={line.quantity}
      />
      <IconButton
        size="small"
        onClick={() => handleChange(value + 1)}
      >
        <PlusIcon fontSize="inherit" />
      </IconButton>
    </div>
  );
};

export default ActiveValue;
