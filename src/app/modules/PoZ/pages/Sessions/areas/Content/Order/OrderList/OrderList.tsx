import React from "react";
import { connect } from "react-redux";

import OrderLine from "../Orderline";

import EmptyList from "./EmptyList";

interface Props {
  olIDs: string[];
}

const OrderList: React.FC<Props> = ({ olIDs }) => {
  return (
    <React.Fragment>
      {olIDs.length === 0 ? (<EmptyList />) : olIDs.map((olID, key) => (
        <OrderLine
          key={key}
          olID={olID}
        />
      ))}
    </React.Fragment>
  );
};

const mapStateToProps = (state: any) => ({
  olIDs: Object.keys(state.PoZ.ActiveOrder.orderlines),
});

const mapDispatchToProps = (dispatch: any) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);
