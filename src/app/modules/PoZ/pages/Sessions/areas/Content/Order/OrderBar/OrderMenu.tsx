import React from "react";

import IconButton from "@material-ui/core/IconButton";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import DotsVerticalIcon from "mdi-material-ui/DotsVertical";

const useClasses = makeStyles(() =>
  createStyles({
    icon: {
      color: "#346",
    },
  }),
);

interface Props {
}

const OrderMenu: React.FC<Props> = () => {
  const classes = useClasses();

  return (
    <IconButton size="small">
      <DotsVerticalIcon className={classes.icon} />
    </IconButton>
  );
};

export default OrderMenu;
