import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { useTranslation } from "react-i18next";

import Column from "./Column";

const useClasses = makeStyles(() =>
  createStyles({
    hrow: {
      backgroundColor: "#ccc",
      display: "flex",
      padding: "8px 0",
      width: "100%",
    },
  }),
);

const useCols = makeStyles(() =>
  createStyles({
    quantity: {
      minWidth: "64px",
      textAlign: "right",
    },
    title: {
      flex: 1,
      textAlign: "center",
    },
    discount: {
      minWidth: "128px",
      textAlign: "right",
    },
    price: {
      minWidth: "128px",
      textAlign: "right",
    },
    total: {
      minWidth: "136px",
      textAlign: "right",
    },
  }),
);

export interface Props {
}

const OrderHeader: React.FC<Props> = () => {
  const classes = useClasses();
  const cols = useCols();
  const { t } = useTranslation("poz");

  return (
    <div className={classes.hrow}>
      <Column className={cols.quantity}>{t("orderline.quantity")}</Column>
      <Column className={cols.title}>{t("orderline.title")}</Column>
      <Column className={cols.discount}>{t("orderline.discount")}</Column>
      <Column className={cols.price}>{t("orderline.price")}</Column>
      <Column className={cols.total}>{t("orderline.total")}</Column>
    </div>
  );
};

export default OrderHeader;
