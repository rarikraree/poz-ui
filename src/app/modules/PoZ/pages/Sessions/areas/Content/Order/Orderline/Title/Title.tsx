import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import Column from "../Column";
import { IOrderline } from "../../../../../../../ducks/States/IOrderline";

const useClasses = makeStyles(() =>
  createStyles({
    column: {
      flex: 1,
      flexDirection: "column",
    },
    title: {
      display: "block",
    },
    subtitle: {
      color: "#666",
      display: "block",
      fontSize: "0.9rem",
    },
    key: {
      fontWeight: 400,
    },
    value: {
      padding: "0 4px",
    },
  }),
);

interface Props {
  isActive: boolean;
  line: IOrderline;
}

const Title: React.FC<Props> = ({ line }) => {
  const classes = useClasses();

  return (
    <Column className={classes.column}>
      <div className={classes.title}>
        {line.title}
      </div>
      <div className={classes.subtitle}>
        <span className={classes.key}>SKU:</span>
        <span className={classes.value}>{line.sku}</span>
      </div>
    </Column>
  );
};

export default Title;
