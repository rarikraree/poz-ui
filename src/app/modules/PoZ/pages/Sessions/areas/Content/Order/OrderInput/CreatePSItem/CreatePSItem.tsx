import React from "react";
import { connect } from "react-redux";

import Button from "@material-ui/core/Button";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { useTranslation } from "react-i18next";

import CreatePSItemDialog from "./CreatePSItemDialog";
import { Command as ToggleNewItemForm } from "../../../../../../../ducks/Actions/ToggleNewItemForm";

const useClasses = makeStyles(() =>
  createStyles({
    button: {
      borderRadius: 0,
      color: "inherit",
      fontWeight: "bold",
    },
    formWrapper: {
      display: "flex",
      flex: 1,
      padding: "0 200px 0 32px",
    },
  }),
);

interface Props {
  isOpen: boolean;
  toggle: (isOpen: boolean) => void;
}

const CreatePSItem: React.FC<Props> = ({ isOpen, toggle }) => {
  const classes = useClasses();
  const { t } = useTranslation("poz");

  return (
    <React.Fragment>
      <Button
        className={classes.button}
        onClick={() => toggle(true)}
      >
        {t("sessions.CreatePSItem")}
      </Button>
      {isOpen &&
        <CreatePSItemDialog
          isOpen={isOpen}
          toggle={toggle}
        />
      }
    </React.Fragment>
  );
};

const mapStateToProps = (state: any) => ({
  isOpen: state.PoZ.IsOpenNewItem || false,
});

const mapDispatchToProps = (dispatch: any) => ({
  toggle: (isOpen: boolean) => dispatch(ToggleNewItemForm({ isOpen })),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreatePSItem);
