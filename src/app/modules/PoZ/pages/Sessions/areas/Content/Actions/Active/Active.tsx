import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import Customer from "./Customer";
import Payment from "./Payment";

const useClasses = makeStyles(() =>
  createStyles({
    wrapper: {
      display: "flex",
      flex: 1,
      flexDirection: "column",
      padding: 8,
    },
    actions: {
      flex: 1,
    },
  }),
);

interface Props {
}

interface IValue {
  customer: {
    ref: string;
  };
}

const Active: React.FC<Props> = () => {
  const classes = useClasses();
  const [state, setState] = React.useState<IValue>({ customer: { ref: "" } });

  return (
    <div className={classes.wrapper}>
      <div className={classes.actions}>
        <Customer
          setState={setState}
          state={state}
        />
      </div>
      <Payment
        state={state}
      />
    </div>
  );
};

export default Active;
