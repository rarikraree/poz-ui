import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { IOrderline } from "../../../../../../../ducks/States/IOrderline";
import Column from "../Column";
import Value from "./Value";

const useClasses = makeStyles(() =>
  createStyles({
    column: {
      flexDirection: "column",
      minWidth: "64px",
    },
  }),
);

interface Props {
  isActive: boolean;
  line: IOrderline;
  removeOrderline: () => void;
  setChanged: (props: { item: IOrderline; src: string; }) => void;
}

const Quantity: React.FC<Props> = ({ isActive, line, removeOrderline, setChanged }) => {
  const classes = useClasses();

  return (
    <Column className={classes.column}>
      {line.quantity &&
        <Value
          isActive={isActive}
          line={line}
          removeOrderline={removeOrderline}
          setChanged={setChanged}
        />
      }
    </Column>
  );
};

export default Quantity;
