import React from "react";
import { connect } from "react-redux";

import Button from "@material-ui/core/Button";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";
import * as Orders from  "../../../../../../../../../services/Orders";

const useClasses = makeStyles(() =>
  createStyles({
    wrapper: {
      display: "flex",
      flex: 1,
      flexDirection: "column",
      padding: 8,
    },
    button: {
      backgroundColor: "#468",
      borderRadius: 0,
      borderWidth: 0,
      color: "#fff",
      fontSize: "1.4rem",
      '&:hover': {
        backgroundColor: "#468",
        borderWidth: 0,
        color: "#fff",
      },
      height: 64,
    },
  }),
);

interface Props {
  state: any;
  order: {
    id: string;
    itemCount: number;
    orderTotal: number;
  };
}

const Payment: React.FC<Props> = ({ state, order }) => {
  const classes = useClasses();
  const { id, itemCount, orderTotal } = order;
  const ref = state.customer.ref;
  const data = { id, ref, itemCount, orderTotal, status: "transaction" };

  return (
    <Button
      className={classes.button}
      variant="outlined"
      onClick={() => {
        Orders.createOrder(data);
        alert("Transaction Sent!")
      }}
    >
      Payment
    </Button>
  );
};

const mapStateToProps = (state: any) => {
  const ActiveOrder = state.PoZ.ActiveOrder;
  const id = ActiveOrder._id;
  const list = Object.keys(ActiveOrder.orderlines).map((key) => {
    const { price, quantity } = ActiveOrder.orderlines[key];
    return { price, quantity };
  });
  const { itemCount, orderTotal } = list.reduce((result, each) => {
    result.itemCount += each.quantity;
    result.orderTotal += each.quantity * each.price;
    return result;
  }, { itemCount: 0, orderTotal: 0 });

  return ({
    order: {
      id,
      itemCount,
      orderTotal,
    },
  });
};

const mapDispatchToProps = (dispatch: any) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Payment);
