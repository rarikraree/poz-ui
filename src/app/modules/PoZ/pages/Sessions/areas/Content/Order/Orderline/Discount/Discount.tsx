import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { IOrderline } from "../../../../../../../ducks/States/IOrderline";
import Column from "../Column";
import ActiveDiscount from "./ActiveDiscount";
import InactiveDiscount from "./InactiveDiscount";

const useClasses = makeStyles(() =>
  createStyles({
    column: {
      flexDirection: "column",
      minWidth: "128px",
      textAlign: "right",
    },
    discount: {
      display: "block",
    },
    percent: {
      color: "#666",
      display: "block",
      fontSize: "0.9rem",
    },
  }),
);

interface Props {
  isActive: boolean;
  line: IOrderline;
  setChanged: (props: { item: IOrderline; src: string; }) => void;
}

const Discount: React.FC<Props> = ({ isActive, line, setChanged }) => {
  const classes = useClasses();

  return (
    <Column className={classes.column}>
      {isActive ?
        (<ActiveDiscount line={line} setChanged={setChanged} />) :
        (<InactiveDiscount line={line} />)}
    </Column>
  );
};

export default Discount;
