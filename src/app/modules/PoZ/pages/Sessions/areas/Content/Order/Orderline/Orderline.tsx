import React from "react";
import { connect } from "react-redux";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { IOrderline } from "../../../../../../ducks/States/IOrderline";
import { Command as RemoveOrderline } from "../../../../../../ducks/Actions/RemoveOrderline";
import { Command as SetOrderlineActive } from "../../../../../../ducks/Actions/SetOrderlineActive";
import { Command as SetOrderlineChanged } from "../../../../../../ducks/Actions/SetOrderlineChanged";
import { Command as SetOrderlineChangedConfirm } from "../../../../../../ducks/Actions/SetOrderlineChangedConfirm";
import { Command as SetOrderlineChangedDiscard } from "../../../../../../ducks/Actions/SetOrderlineChangedDiscard";

import Quantity from "./Quantity";
import Title from "./Title";
import Discount from "./Discount";
import Price from "./Price";
import Total from "./Total";

const useClasses = makeStyles(() =>
  createStyles({
    line: {
      "&:hover": {
        backgroundColor: "#eee",
      },
      borderBottom: "1px solid #ddd",
      cursor: "pointer",
      display: "flex",
      padding: "8px 0",
      width: "100%",
    },
  }),
);

interface Props {
  olID: string;
  hasActive: boolean;
  isActive: boolean;
  item: IOrderline;
  removeOrderline: () => void;
  setActive: (data: { index: number; olID: string; }) => void;
  setChanged: (props: { item: IOrderline; src: string; }) => void;
  setConfirm: () => void;
  setDiscard: () => void;
}

const Orderline: React.FC<Props> = ({ olID, hasActive, isActive, item, removeOrderline, setActive, setChanged, setConfirm, setDiscard }) => {
  const classes = useClasses();
  const data = { index: item.index, olID };
  const handleClick = hasActive || isActive ? undefined : () => setActive(data);

  return (
    <div
      className={classes.line}
      onClick={handleClick}
    >
      <Quantity
        isActive={isActive}
        line={item}
        removeOrderline={removeOrderline}
        setChanged={setChanged}
      />
      <Title
        isActive={isActive}
        line={item}
      />
      <Discount
        isActive={isActive}
        line={item}
        setChanged={setChanged}
      />
      <Price
        isActive={isActive}
        line={item}
      />
      <Total
        isActive={isActive}
        line={item}
        onConfirm={() => setConfirm()}
        onDiscard={() => setDiscard()}
      />
    </div>
  );
};

const mapStateToProps = (state: any, { olID }: any) => ({
  hasActive: state.PoZ.ActiveOrder.olActiveID && true,
  isActive: state.PoZ.ActiveOrder.olActiveID === olID,
  item: state.PoZ.ActiveOrder.orderlines[olID],
});

const mapDispatchToProps = (dispatch: any, { olID }: any) => ({
  removeOrderline: () => dispatch(RemoveOrderline({ olID })),
  setActive: (data: { index: number; olID: string; }) => dispatch(SetOrderlineActive(data)),
  setChanged: (props: { item: IOrderline; src: string; }) => dispatch(SetOrderlineChanged(props)),
  setConfirm: () => dispatch(SetOrderlineChangedConfirm()),
  setDiscard: () => dispatch(SetOrderlineChangedDiscard()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Orderline);
