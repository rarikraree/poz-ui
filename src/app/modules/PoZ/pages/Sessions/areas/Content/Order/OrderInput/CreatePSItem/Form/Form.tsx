import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import Field from "./Field";
import { IForm } from "./IForm";
import { IFieldData } from "./IFormField";

const useClasses = makeStyles(() =>
  createStyles({
    form: {
      display: "flex",
      flex: 1,
      flexDirection: "column",
    },
  }),
);

interface Props {
  form: IForm;
  onFieldChange?: (fieldChange: { field: IFieldData; key: string; value: any; }) => void;
}

const Form: React.FC<Props> = ({ form, onFieldChange }) => {
  const classes = useClasses();
  const fields: any[] = Object.keys(form).map((key) => Object.assign(
    {},
    { key },
    form[key],
    {
      isRequired: form[key].isRequired ? true : false,
      value: form[key].value ? form[key].value : form[key].type === "number" ? 0 : "",
    }
  ));

  return (
    <div className={classes.form}>
      {fields.map((each) => (
        <Field
          key={each.key}
          field={each}
          onFieldChange={onFieldChange}
        />
      ))}
    </div>
  );
};

export default Form;
