import React from "react";
import clsx from "clsx";
import { connect } from "react-redux";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { useTranslation } from "react-i18next";

import { currency } from "../../../../../../../../common/formatters";
import { resolveTotal } from "../Orderline/Total";

const useClasses = makeStyles((theme) =>
  createStyles({
    ordertotal: {
      backgroundColor: "#ccc",
      borderRadius: "0 0 8px 8px",
      display: "flex",
      padding: "8px 0",
    },
    text: {
      padding: "0 8px",
      fontSize: "1.04rem",
    },
    bold: {
      fontWeight: "bold",
    },
    space: {
      flex: 1,
    },
  }),
);

export interface Props {
  total: number;
}

const OrderTotal: React.FC<Props> = ({ total }) => {
  const classes = useClasses();
  const { t } = useTranslation("poz");
  const tTotal = currency(total);

  return (
    <div className={classes.ordertotal}>
      <span className={classes.space} />
      <div>
        <span className={classes.text}>
          {t("orderline.ordertotal")}
        </span>
        <span className={clsx(classes.text, classes.bold)}>
          {tTotal}
        </span>
      </div>
    </div>
  );
};

const mapStateToProps = (state: any) => {
  const ActiveOrder = state.PoZ.ActiveOrder;
  const orderlines = Object.keys(ActiveOrder.orderlines).map((olID) => ActiveOrder.orderlines[olID]);
  const total = orderlines.reduce((result, each) => result + resolveTotal(each).total, 0);

  return {
    total,
  };
};

const mapDispatchToProps = (dispatch: any) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderTotal);
