import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { currency } from "../../../../../../../../../common/formatters/currency";
import { IOrderline } from "../../../../../../../ducks/States/IOrderline";
import Column from "../Column";

const useClasses = makeStyles(() =>
  createStyles({
    column: {
      flexDirection: "column",
      minWidth: "128px",
      textAlign: "right",
    },
    price: {
      display: "block",
    },
    listing: {
      color: "#f33",
      display: "block",
      fontSize: "0.9rem",
      textDecoration: "line-through",
    },
  }),
);

interface Props {
  isActive: boolean;
  line: IOrderline;
}

export const resolvePrice = (orderline: any) => {
  const { discount, price } = orderline;
  const discountAmount = discount ? discount.amount : undefined;
  let initial: undefined | number = undefined;
  let sale: number = price;

  if (discountAmount) {
    initial = price;
    sale = sale - discountAmount;
  }

  return {
    sale,
    initial,
  };
};

const Price: React.FC<Props> = ({ line }) => {
  const classes = useClasses();
  const { initial, sale } = resolvePrice(line);

  const tInitial = initial ? currency(initial) : undefined;
  const tSale = currency(sale);

  return (
    <Column className={classes.column}>
      <div className={classes.price}>
        {tSale}
      </div>
      <div className={classes.listing}>
        {tInitial}
      </div>
    </Column>
  );
};

export default Price;
