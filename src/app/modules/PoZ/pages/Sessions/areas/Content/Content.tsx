import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import Order from "./Order";
import Actions from "./Actions";

const useClasses = makeStyles(() =>
  createStyles({
    content: {
      display: "flex",
      paddingTop: "36px",
      height: "100%",
      width: "100%",
    },
    main: {
      flex: 1,
    },
    actions: {
      backgroundColor: "#fff",
      borderLeft: "1px solid #ddd",
      display: "flex",
      flexDirection: "column",
      height: "100%",
      maxWidth: "600px",
      width: "35%",
    },
  }),
);

const Session: React.FC = () => {
  const classes = useClasses();

  return (
    <div className={classes.content}>
      <div className={classes.main}>
        <Order />
      </div>
      <div className={classes.actions}>
        <Actions />
      </div>
    </div>
  );
};

export default Session;
