import React from "react";
import { useSelector } from "react-redux";

import Typography from "@material-ui/core/Typography";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import Initial from "./Initiate";
import ActiveOrder from "./Active";
import { IPoZState } from "../../../../../ducks/IPoZState";

const useClasses = makeStyles(() =>
  createStyles({
    wrapper: {
      display: "flex",
      flex: 1,
      flexDirection: "column",
      padding: 0,
    },
    titleWrapper: {
      padding: "8px 8px",
    },
    title: {
      color: "#d22", // "#346",
      fontSize: "1.4rem",
      fontWeight: 600,
      textTransform: "uppercase",
    },
    actionsWrapper: {
      display: "flex",
      flex: 1,
      flexDirection: "column",
      padding: "0 8px",
    },
  }),
);

interface Props {
}

const Actions: React.FC<Props> = () => {
  const classes = useClasses();
  const order = useSelector((state: { PoZ: IPoZState; }) => state.PoZ.ActiveOrder);

  return (
    <div className={classes.wrapper}>
      <div className={classes.titleWrapper}>
        <Typography>
          <span className={classes.title}>WOW at Central</span>
        </Typography>
      </div>
      <div className={classes.actionsWrapper}>
        {!order ?
          (<Initial />) :
          (<ActiveOrder />)
        }
      </div>
    </div>
  );
};

export default Actions;
