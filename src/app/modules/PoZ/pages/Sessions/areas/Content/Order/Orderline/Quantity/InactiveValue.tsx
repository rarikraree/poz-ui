import React from "react";

import { IOrderline } from "../../../../../../../ducks/States/IOrderline";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useClasses = makeStyles(() =>
  createStyles({
    wrapper: {
      flex: 1,
      textAlign: "right",
    },
  }),
);

interface Props {
  line: IOrderline;
}

const InactiveValue: React.FC<Props> = ({ line }) => {
  const classes = useClasses();

  return (
    <div className={classes.wrapper}>
      {line.quantity}
    </div>
  );
};

export default InactiveValue;
