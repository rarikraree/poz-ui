import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { IFieldData } from "./IFormField";
import SelectField from "./SelectField";
import TextNumberField from "./TextNumberField";

const useClasses = makeStyles(() =>
  createStyles({
    row: {
      flex: 1,
      padding: "8px 0 20px",
    },
  }),
);

interface Props {
  field: IFieldData;
  onFieldChange?: (fieldChange: { field: IFieldData; key: string; value: any; }) => void;
}

const Field: React.FC<Props> = ({ field, onFieldChange, ...rest }) => {
  const classes = useClasses();
  let FieldComponent;

  switch (field.type) {
    case "select":
      FieldComponent = SelectField;
      break;

    default:
      FieldComponent = TextNumberField;
      break;
  }

  return (
    <div className={classes.row}>
      <FieldComponent
        field={field}
        onFieldChange={onFieldChange}
        {...rest}
      />
    </div>
  );
};

export default Field;
