import React from "react";
import { useDispatch } from "react-redux";

import Button from "@material-ui/core/Button";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { useTranslation } from "react-i18next";

import { Command as InitiateNewOrder } from "../../../../../../ducks/Actions/InitiateNewOrder";

const useClasses = makeStyles(() =>
  createStyles({
    button: {
      backgroundColor: "#eee",
      borderRadius: 0,
      color: "#444",
      fontSize: "1.1rem",
    },
  }),
);

interface Props {
}

const Initiate: React.FC<Props> = () => {
  const classes = useClasses();
  const { t } = useTranslation("poz");
  const dispatch = useDispatch();

  return (
    <Button
      className={classes.button}
      variant="outlined"
      onClick={() => dispatch(InitiateNewOrder())}
    >
      {t("sessions.InitiateOrder")}
    </Button>
  );
};

export default Initiate;
