import React from "react";

import Typography from "@material-ui/core/Typography";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useClasses = makeStyles(() =>
  createStyles({
    topbar: {
      backgroundColor: "#346",
      color: "#fff",
      display: "flex",
      flexDirection: "row",
      height: "36px",
      padding: 0,
      position: "absolute",
      top: 0,
      width: "100%",
    },
    titleWrapper: {
      display: "inline-block",
      height: "36px",
      padding: " 0 32px",
      userSelect: "none",
    },
    title: {
      color: "#fff",
      fontSize: "1.4rem",
      fontWeight: 800,
      padding: "2px 0  0 16px",
    },
    contentWrapper: {
      display: "flex",
      flex: 1,
      height: "36px",
      padding: 0,
      userSelect: "none",
    },
  }),
);

const TopBar = () => {
  const classes = useClasses();

  return (
    <div className={classes.topbar}>
      <div className={classes.titleWrapper}>
        <Typography className={classes.title}>
          PoZ
        </Typography>
      </div>
      <div className={classes.contentWrapper} />
    </div>
  );
};

export default TopBar;
