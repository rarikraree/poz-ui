import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import TopBar from "./areas/TopBar";
import Content from "./areas/Content";

const useClasses = makeStyles(() =>
  createStyles({
    page: {
      backgroundColor: "#ddd",
      display: "flex",
      flexDirection: "column",
      height: "100%",
    },
    pagecontent: {
      display: "flex",
      height: "100%",
      paddingTop: "36px",
    },
  }),
);

const Page: React.FC = () => {
  const classes = useClasses();

  return (
    <div className={classes.page}>
      <TopBar />
      <Content />
    </div>
  );
};

export default Page;
