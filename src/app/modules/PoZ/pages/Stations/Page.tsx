import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import AppToolbar from "../../../../common/components/AppToolbar";

const useClasses = makeStyles((theme) =>
  createStyles({
    page: {
      display: "flex",
      flex: 1,
      flexDirection: "column",
    },
  }),
);

const Page: React.FC = () => {
  const classes = useClasses();

  return (
    <div
      className={classes.page}
    >
      <AppToolbar
        title="PoS"
      />
    </div>
  );
};

export default Page;
