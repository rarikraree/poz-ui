import React from "react";

import Typography from "@material-ui/core/Typography";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import FlatButton from "../../../../../../../common/components/FlatButton";

const useClasses = makeStyles(() =>
  createStyles({
    wrapper: {
      backgroundColor: "#fff",
      borderBottom: "1px solid #ccc",
      color: "#666",
      display: "flex",
      flexDirection: "column",
      padding: "0",
      width: "100%",
    },
    title: {
      color: "inherit",
      display: "inline",
      padding: "0",
      userSelect: "none",
    },
    row1: {
      padding: "8px 24px",
    },
    row2: {
      padding: "0 24px 8px",
    },
  }),
);

const SessionBar: React.FC = () => {
  const classes = useClasses();

  return (
    <div className={classes.wrapper}>
      <div className={classes.row1}>
        <Typography
          className={classes.title}
          variant="h6"
        >Station Name</Typography>
      </div>
      <div className={classes.row2}>
        <FlatButton>Create New</FlatButton>
      </div>
    </div>
  );
};

export default SessionBar;
