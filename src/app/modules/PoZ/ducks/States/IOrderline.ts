export interface IOrderline {
  index: number;
  id: string;
  ps: {
    _id: string;
    _version: number;
  };
  sku: string;
  title: string;
  type: string;
  quantity?: number;
  price: number;
  discount?: {
    amount: number;
    percent: number;
    value: string;
  };
}
