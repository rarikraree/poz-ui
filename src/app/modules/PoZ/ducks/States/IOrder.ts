import { IOrderline } from "./IOrderline";

export interface IOrder {
  _id: string;
  transationAt: Date;
  orderlines: {
    [id: string]: IOrderline;
  };
  olCount: number;
  olActiveID?: string;
  olInitialState?: IOrderline;
}
