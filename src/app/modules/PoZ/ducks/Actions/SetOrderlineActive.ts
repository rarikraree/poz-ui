import { createAction, IActionHandler, IActionWithPayload } from "reduxes";
import { IPoZState } from "../IPoZState";

interface ISetOrderlineActiveActionPayloads {
  index: number;
  olID: string;
}

export interface ISetOrderlineActiveAction extends IActionWithPayload<ISetOrderlineActiveActionPayloads> { };

export const SET_ORDERLINE_ACTIVE = "POZ/SET_ORDERLINE_ACTIVE";

export const Command = (data: { index: number; olID: string; }): ISetOrderlineActiveAction => createAction<ISetOrderlineActiveActionPayloads>(
  SET_ORDERLINE_ACTIVE,
  data,
);

export const ActionHandler: IActionHandler<IPoZState, ISetOrderlineActiveAction> = {
  [SET_ORDERLINE_ACTIVE]: (state, action) => {
    const { olID } = action.payload;

    const ActiveOrder: any = state.ActiveOrder;
    const activeOrder = {
      ...ActiveOrder,
      olActiveID: olID,
      olInitialState: Object.assign({}, ActiveOrder.orderlines[olID])
    };

    return Object.assign({}, state, {
      ActiveOrder: activeOrder,
    });
  },
};
