import { createAction, IAction, IActionHandler } from "reduxes";
import { IPoZState } from "../IPoZState";

export interface ISetOrderlineChangedConfirmAction extends IAction { };

export const SET_ORDERLINE_CHANGED_CONFIRM = "POZ/SET_ORDERLINE_CHANGED_CONFIRM";

export const Command = (): ISetOrderlineChangedConfirmAction => createAction(
  SET_ORDERLINE_CHANGED_CONFIRM
);

export const ActionHandler: IActionHandler<IPoZState, ISetOrderlineChangedConfirmAction> = {
  [SET_ORDERLINE_CHANGED_CONFIRM]: (state) => {
    const ActiveOrder: any = state.ActiveOrder;

    const activeOrder = {
      ...ActiveOrder,
      olActiveID: undefined,
      olInitialState: undefined,
    };

    return Object.assign({}, state, {
      ActiveOrder: activeOrder,
    });
  },
};
