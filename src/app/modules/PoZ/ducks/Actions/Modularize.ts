import { createAction, IAction, IActionHandler } from "reduxes";
import { IPoZState } from "../IPoZState";

export interface IModularizeAction extends IAction { };

export const MODULARIZE = "POZ/MODULARIZE";

export const Command = (): IModularizeAction => createAction(MODULARIZE);

export const ActionHandler: IActionHandler<IPoZState, IModularizeAction> = {
  [MODULARIZE]: (state, action) => Object.assign({}, state, {
    Data: {},
  }),
};
