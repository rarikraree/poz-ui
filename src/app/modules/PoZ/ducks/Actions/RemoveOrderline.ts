import { createAction, IActionWithPayload, IActionHandler } from "reduxes";
import { IPoZState } from "../IPoZState";

interface IRemoveOrderlineActionPayloads {
  olID: string;
}

export interface IRemoveOrderlineAction extends IActionWithPayload<IRemoveOrderlineActionPayloads> { };

export const REMOVE_ORDERLINE = "POZ/REMOVE_ORDERLINE";

export const Command = (data: { olID: string; }): IRemoveOrderlineAction => createAction<IRemoveOrderlineActionPayloads>(
  REMOVE_ORDERLINE,
  data,
);

export const ActionHandler: IActionHandler<IPoZState, IRemoveOrderlineAction> = {
  [REMOVE_ORDERLINE]: (state, action) => {
    const { olID } = action.payload;
    const ActiveOrder: any = state.ActiveOrder;
    const activeOrder = {
      ...ActiveOrder,
      orderlines: {
        ...ActiveOrder.orderlines,
      },
      olCount: ActiveOrder.olCount - 1,
      olActiveID: undefined,
      olInitialState: undefined,
    };

    delete activeOrder.orderlines[olID];

    return Object.assign({}, state, {
      ActiveOrder: activeOrder,
    });
  },
};
