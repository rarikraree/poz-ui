import { createAction, IActionWithPayload, IActionHandler } from "reduxes";
import { IPoZState } from "../IPoZState";
import * as UUID from "uuid";
import { IOrder } from "../States/IOrder";

interface IInitiateNewOrderActionPayloads {
  aggregateID: string;
  transationAt: Date;
}

export interface IInitiateNewOrderAction extends IActionWithPayload<IInitiateNewOrderActionPayloads> { };

export const INITIATE_NEW_ORDER = "POZ/INITIATE_NEW_ORDER";

export const Command = (data: { customerMobile?: string; transationAt?: Date; } = {}): IInitiateNewOrderAction => createAction<IInitiateNewOrderActionPayloads>(
  INITIATE_NEW_ORDER,
  {
    aggregateID: UUID.v4(),
    transationAt: data.transationAt || new Date(),
  },
);

export const ActionHandler: IActionHandler<IPoZState, IInitiateNewOrderAction> = {
  [INITIATE_NEW_ORDER]: (state, action) => {
    const { aggregateID, transationAt } = action.payload;

    const ActiveOrder: IOrder = {
      _id: aggregateID,
      transationAt,
      orderlines: {},
      olCount: 0,
    };

    return Object.assign({}, state, {
      ActiveOrder,
    });
  },
};
