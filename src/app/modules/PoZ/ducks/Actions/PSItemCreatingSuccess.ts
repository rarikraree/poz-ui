import { createAction, IActionWithPayload, IActionHandler } from "reduxes";
import { IPoZState } from "../IPoZState";
import { closeNewItemFormState } from "./ToggleNewItemForm";
import { IOrderline } from "../States/IOrderline";
import { toOrderline } from "./common/toOrderline";

interface IPSItemCreatingSuccessActionPayloads {
  _id: string;
  _version: string;
  barcode?: string;
  price: number,
  sku: string;
  title: string;
  type: string;
}

export interface IPSItemCreatingSuccessAction extends IActionWithPayload<IPSItemCreatingSuccessActionPayloads> { };

export const PSITEM_CREATING_SUCCESS = "POZ/PSITEM_CREATING_SUCCESS";

export const Command = (
  data: any,
): IPSItemCreatingSuccessAction => createAction<IPSItemCreatingSuccessActionPayloads>(
  PSITEM_CREATING_SUCCESS,
  data,
);

export const ActionHandler: IActionHandler<IPoZState, IPSItemCreatingSuccessAction> = {
  [PSITEM_CREATING_SUCCESS]: (state, action) => {
    const item = action.payload;
    const PS: any = state.Data.PS || {};

    const ps = Object.assign({}, PS, {
      ...PS,
      ids: {
        ...PS.ids,
        [item._id]: item,
      },
      queries: {
        ...PS.queries,
        [item.sku]: {
          type: "sku",
          id: item._id,
        },
      },
    });

    const orderline: IOrderline = {
      ...toOrderline(item),
      index: state.ActiveOrder!.olCount,
      quantity: 1,
    };

    const ActiveOrder: any = state.ActiveOrder;

    const activeOrder = {
      ...ActiveOrder,
      orderlines: {
        ...ActiveOrder.orderlines,
        [item._id]: orderline,
      },
      olCount: orderline.index + 1,
    };

    const newItemFormState = closeNewItemFormState();

    return Object.assign({}, state,
      {
        ActiveOrder: activeOrder,
        Data: {
          ...state.Data,
          PS: ps,
        },
      },
      newItemFormState,
    );
  },
};
