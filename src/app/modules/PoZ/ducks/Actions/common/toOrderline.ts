export const toOrderline = (item: any) => {
  const orderline = {
    id: item._id,
    ps: {
      _id: item._id,
      _version: item._version,
    },
    sku: item.sku,
    title: item.title,
    type: item.type,
    price: item.price,
  };
  
  return orderline;
};
