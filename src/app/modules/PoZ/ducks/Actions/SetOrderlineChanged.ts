import { createAction, IActionWithPayload, IActionHandler } from "reduxes";
import { IPoZState } from "../IPoZState";
import { IOrderline } from "../States/IOrderline";

interface ISetOrderlineChangedActionPayloads {
  orderline: IOrderline
  src: string;
}

export interface ISetOrderlineChangedAction extends IActionWithPayload<ISetOrderlineChangedActionPayloads> { };

export const SET_ORDERLINE_CHANGED = "POZ/SET_ORDERLINE_CHANGED";

export const Command = (data: { item: IOrderline; src: string; }): ISetOrderlineChangedAction => createAction<ISetOrderlineChangedActionPayloads>(
  SET_ORDERLINE_CHANGED,
  {
    orderline: data.item,
    src: data.src,
  },
);

export const ActionHandler: IActionHandler<IPoZState, ISetOrderlineChangedAction> = {
  [SET_ORDERLINE_CHANGED]: (state, action) => {
    const { orderline } = action.payload;
    const ActiveOrder: any = state.ActiveOrder;
    const activeOrder = {
      ...ActiveOrder,
      orderlines: {
        ...ActiveOrder.orderlines,
        [orderline.ps._id]: orderline,
      },
    };

    return Object.assign({}, state, {
      ActiveOrder: activeOrder,
    });
  },
};
