import { createAction, IActionWithPayload, IActionHandler } from "reduxes";
import * as uuid from "uuid";
import { IPoZState } from "../IPoZState";

interface IToggleNewItemFormActionPayloads {
  isOpen: boolean;
}

export interface IToggleNewItemFormAction extends IActionWithPayload<IToggleNewItemFormActionPayloads> { };

export const TOGGLE_NEW_ITEM_FORM = "POZ/TOGGLE_NEW_ITEM_FORM";

export const Command = (data: { isOpen: boolean; }): IToggleNewItemFormAction => createAction<IToggleNewItemFormActionPayloads>(
  TOGGLE_NEW_ITEM_FORM,
  data,
);

export const ActionHandler: IActionHandler<IPoZState, IToggleNewItemFormAction> = {
  [TOGGLE_NEW_ITEM_FORM]: (state, action) => {
    const newItemFormState = state.IsOpenNewItem ? closeNewItemFormState() : openNewItemFormState();

    return Object.assign({}, state, newItemFormState);
  },
};

export const openNewItemFormState = (options: {
  _id?: string;
  type?: string;
  sku?: string;
  title?: string;
  price?: string;
  barcode?: string;
  vendor?: string;
} = {}) => ({
  IsOpenNewItem: true,
  NewItem: {
    _id: options._id || uuid.v4(),
    type: options.type || "product",
    sku: options.sku || "",
    title: options.title || "",
    price: options.price || "1.00",
    barcode: options.barcode || "",
    vendor: options.vendor || "",
  },
});

export const closeNewItemFormState = () => ({
  IsOpenNewItem: false,
  NewItem: undefined,
  OrderlineQuery: undefined,
});
