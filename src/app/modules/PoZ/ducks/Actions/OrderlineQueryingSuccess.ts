import { createAction, IActionWithPayload, IActionHandler } from "reduxes";
import { IPoZState } from "../IPoZState";
import { openNewItemFormState } from "./ToggleNewItemForm";
import { IOrderline } from "../States/IOrderline";
import { toOrderline } from "./common/toOrderline";

interface IOrderlineQueryingSuccessActionPayloads {
  raw: string;
  quantity: number;
  query: string;
  result: {
    type: string;
    item: any;
  };
}

export interface IOrderlineQueryingSuccessAction extends IActionWithPayload<IOrderlineQueryingSuccessActionPayloads> { };

export const ORDERLINE_QUERYING_SUCCESS = "POZ/ORDERLINE_QUERYING_SUCCESS";

export const Command = (
  data: {
    quantity: number;
    query: string;
    raw: string;
    result: {
      type: string;
      item: any;
    };
  },
): IOrderlineQueryingSuccessAction => createAction<IOrderlineQueryingSuccessActionPayloads>(
  ORDERLINE_QUERYING_SUCCESS,
  data,
);

export const ActionHandler: IActionHandler<IPoZState, IOrderlineQueryingSuccessAction> = {
  [ORDERLINE_QUERYING_SUCCESS]: (state, action) => {
    const { quantity, query, raw, result } = action.payload;

    if (result && result.item) {
      const { item, type } = result;
      const PS: any = state.Data.PS || {};

      const ps = Object.assign({}, PS, {
        ...PS,
        ids: {
          ...PS.ids,
          [item._id]: item,
        },
        queries: {
          ...PS.queries,
          [query]: {
            type,
            id: item._id,
          },
        },
      });

      const orderline: IOrderline = {
        ...toOrderline(item),
        index: state.ActiveOrder!.olCount,
        quantity,
      };

      const ActiveOrder: any = state.ActiveOrder;

      const activeOrder = {
        ...ActiveOrder,
        orderlines: {
          ...ActiveOrder.orderlines,
          [item._id]: orderline,
        },
        olCount: orderline.index + 1,
      };

      return Object.assign({}, state, {
        ActiveOrder: activeOrder,
        Data: {
          ...state.Data,
          PS: ps,
        },
      });
    } else {
      const orderlineQueryState = {
        OrderlineQuery: {
          raw,
          quantity,
          query,
        },
      };

      const newItemFormState = openNewItemFormState({
        sku: query,
      });

      return Object.assign({}, state, orderlineQueryState, newItemFormState);
    }
  },
};
