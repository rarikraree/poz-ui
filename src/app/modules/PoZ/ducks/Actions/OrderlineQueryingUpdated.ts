import { createAction, IActionWithPayload, IActionHandler } from "reduxes";
import { IPoZState } from "../IPoZState";
import { toOrderline } from "./common/toOrderline";

interface IOrderlineQueryingUpdatedActionPayloads {
  raw: string;
  quantity: number;
  query: string;
  olID: string;
}

export interface IOrderlineQueryingUpdatedAction extends IActionWithPayload<IOrderlineQueryingUpdatedActionPayloads> { };

export const ORDERLINE_QUERYING_UPDATED = "POZ/ORDERLINE_QUERYING_UPDATED";

export const Command = (
  data: {
    olID: string;
    quantity: number;
    query: string;
    raw: string;
  },
): IOrderlineQueryingUpdatedAction => createAction<IOrderlineQueryingUpdatedActionPayloads>(
  ORDERLINE_QUERYING_UPDATED,
  data,
);

export const ActionHandler: IActionHandler<IPoZState, IOrderlineQueryingUpdatedAction> = {
  [ORDERLINE_QUERYING_UPDATED]: (state, action) => {
    const { olID, quantity } = action.payload;
    const ActiveOrder: any = state.ActiveOrder;
    const PS: any = state.Data.PS;
    const Orderline = ActiveOrder.orderlines[olID] || toOrderline(PS.ids[olID]);

    const qty = (Orderline.quantity || 0) + quantity;
    const orderline = Object.assign({}, Orderline, {
      quantity: qty,
    });


    const activeOrder = {
      ...ActiveOrder,
      orderlines: {
        ...ActiveOrder.orderlines,
        [olID]: orderline,
      },
    };

    return Object.assign({}, state, {
      ActiveOrder: activeOrder,
    });
  },
};
