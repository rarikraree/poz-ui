import { createAction, IAction, IActionHandler } from "reduxes";
import { IPoZState } from "../IPoZState";

export interface ISetOrderlineChangedDiscardAction extends IAction { };

export const SET_ORDERLINE_CHANGED_DISCARD = "POZ/SET_ORDERLINE_CHANGED_DISCARD";

export const Command = (): ISetOrderlineChangedDiscardAction => createAction(
  SET_ORDERLINE_CHANGED_DISCARD
);

export const ActionHandler: IActionHandler<IPoZState, ISetOrderlineChangedDiscardAction> = {
  [SET_ORDERLINE_CHANGED_DISCARD]: (state) => {
    const ActiveOrder: any = state.ActiveOrder;
    const olInitialState: any = ActiveOrder.olInitialState;

    const activeOrder = {
      ...ActiveOrder,
      orderlines: {
        ...ActiveOrder.orderlines,
        [olInitialState.id]: olInitialState,
      },
      olActiveID: undefined,
      olInitialState: undefined,
    };

    return Object.assign({}, state, {
      ActiveOrder: activeOrder,
    });
  },
};
