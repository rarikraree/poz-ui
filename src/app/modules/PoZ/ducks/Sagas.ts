import { takeLatest } from "redux-saga/effects";

import { ORDERLINE_QUERYING, sagaActionHandler as OrderLineQuerying } from "./SagaActions/OrderlineQuerying";
import { PSITEM_CREATING, sagaActionHandler as PSItemCreating } from "./SagaActions/PSItemCreating";

function* Sagas() {
  yield takeLatest(ORDERLINE_QUERYING, OrderLineQuerying);
  yield takeLatest(PSITEM_CREATING, PSItemCreating);
}

export default Sagas;
