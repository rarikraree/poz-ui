import { createAction, IActionWithPayload } from "reduxes";
import { put } from "redux-saga/effects";
import * as UUID from "uuid";

import * as PSS from "../../../../services/PSS";
import { Command as PSItemCreatingSuccess } from "../Actions/PSItemCreatingSuccess";

interface IPSItemCreatingActionPayloads {
  id: string;
  sku: string;
  type: string;
  title: string;
  price: number;
  vendor: string;
  barcode?: string;
}

export interface IPSItemCreatingAction extends IActionWithPayload<IPSItemCreatingActionPayloads> { };

export const PSITEM_CREATING = "POZ/PSITEM_CREATING";

export const Command = (data: {
  id?: string;
  barcode?: string;
  price: number,
  sku: string;
  title: string;
  vendor: string;
  type: string;
}): IPSItemCreatingAction => createAction<IPSItemCreatingActionPayloads>(
  PSITEM_CREATING,
  {
    id: data.id || UUID.v4(),
    barcode: data.barcode && data.barcode.length > 0 ? data.barcode : undefined,
    price: data.price,
    sku: data.sku,
    title: data.title,
    vendor: data.vendor,
    type: data.type,
  },
);

export function* sagaActionHandler(action: IPSItemCreatingAction) {
  const item = action.payload;
  const result = yield PSS.createPSItem(item);

  if (result.success) {
    yield put(PSItemCreatingSuccess(result.data));
  }
}
