import { createAction, IActionWithPayload } from "reduxes";
import { put, select } from "redux-saga/effects";

import * as PSS from "../../../../services/PSS";
import { Command as OrderlineQueryingUpdated } from "../Actions/OrderlineQueryingUpdated";
import { Command as OrderlineQueryingSuccess } from "../Actions/OrderlineQueryingSuccess";

interface IOrderlineQueryingActionPayloads {
  query: string;
}

export interface IOrderlineQueryingAction extends IActionWithPayload<IOrderlineQueryingActionPayloads> { };

export const ORDERLINE_QUERYING = "POZ/ORDERLINE_QUERYING";

export const Command = (data: { query: string; }): IOrderlineQueryingAction => createAction<IOrderlineQueryingActionPayloads>(
  ORDERLINE_QUERYING,
  {
    query: data.query,
  },
);

const selectQueryID = (query: string) => (state: any) => {
  const { PS } = state.PoZ.Data;
  if (PS && PS.queries[query]) {
    const { id } = PS.queries[query];
    return id;
  } else {
    return undefined;
  }
};

export function* sagaActionHandler(action: IOrderlineQueryingAction) {
  const raw = action.payload.query;
  let quantity = 1;
  let query = "";
  const vals = raw.split(",").map((each) => each.trim());

  if (vals.length === 1) {
    quantity = 1;
    query = vals[0];
  } else if (vals.length === 2) {
    const n = Number(vals[0]);
    quantity = Number.isNaN(n) ? 1 : n;
    query = vals[1];
  }

  const olID = yield select(selectQueryID(query));

  if (olID) {
    yield put(OrderlineQueryingUpdated({ olID, quantity, query, raw }));
  } else {
    const result = yield PSS.query({ query });

    if (result.success) {
      yield put(OrderlineQueryingSuccess({ quantity, query, raw, result: result.data }));
    }
  }
}
