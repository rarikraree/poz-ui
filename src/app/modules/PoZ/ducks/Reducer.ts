import { createReducer } from "reduxes";
import { IPoZState } from "./IPoZState";

import { ActionHandler as InitiateNewOrder } from "./Actions/InitiateNewOrder";
import { ActionHandler as Modularize } from "./Actions/Modularize";
import { ActionHandler as OrderlineQueryingSuccess } from "./Actions/OrderlineQueryingSuccess";
import { ActionHandler as OrderlineQueryingUpdated } from "./Actions/OrderlineQueryingUpdated";
import { ActionHandler as PSItemCreatingSuccess } from "./Actions/PSItemCreatingSuccess";
import { ActionHandler as RemoveOrderline } from "./Actions/RemoveOrderline";
import { ActionHandler as SetOrderlineActive } from "./Actions/SetOrderlineActive";
import { ActionHandler as SetOrderlineChanged } from "./Actions/SetOrderlineChanged";
import { ActionHandler as SetOrderlineChangedConfirm } from "./Actions/SetOrderlineChangedConfirm";
import { ActionHandler as SetOrderlineChangedDiscard } from "./Actions/SetOrderlineChangedDiscard";
import { ActionHandler as ToggleNewItemForm } from "./Actions/ToggleNewItemForm";

const defaultState: IPoZState = {
  Data: {},
}

const Reducer = createReducer(
  defaultState,
  InitiateNewOrder,
  Modularize,
  OrderlineQueryingSuccess,
  OrderlineQueryingUpdated,
  PSItemCreatingSuccess,
  RemoveOrderline,
  SetOrderlineActive,
  SetOrderlineChanged,
  SetOrderlineChangedConfirm,
  SetOrderlineChangedDiscard,
  ToggleNewItemForm,
);

export default Reducer;
