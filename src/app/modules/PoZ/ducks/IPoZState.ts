import { IOrder } from "./States/IOrder";

export interface IPoZState {
  Data: {
    PS?: {
      ids: {
        [id: string]: any;
      };
      queries: {
        [query: string]: {
          type: string;
          id: string;
        };
      };
    };
  };
  ActiveOrder?: IOrder;
  PinOrders?: {
    [OrderID: string]: IOrder;
  };
  OrderlineQuery?: {
    raw: string;
    quantity: number;
    query: string;
  };
  IsOpenNewItem?: boolean;
  NewItem?: {
    _id: string;
    type: string;
    sku: string;
    title: string;
    price: string;
    barcode: string | undefined;
    vendor: string;
  };
};
