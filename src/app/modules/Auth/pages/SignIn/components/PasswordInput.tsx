import React, { useState } from "react";

import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import IconButton from "@material-ui/core/IconButton";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import InputLabel from "@material-ui/core/InputLabel";

import Visibility from "mdi-material-ui/Eye";
import VisibilityOff from "mdi-material-ui/EyeOff";

export interface Props {
  id?: string;
  className?: string;
  error?: boolean;
  helperText?: string;
  margin?: "none" | "normal" | "dense";
  label: string;
  value: string;
}

const PasswordInput: React.FC<Props> = ({ id = "adornment-password", className, error, helperText, margin, label, ...rest }) => {
  const [isVisible, setVisible] = useState(false);

  return (
    <FormControl
      className={className}
      margin={margin}
    >
      <InputLabel
        htmlFor={id}
        error={error}
      >
        {label}
      </InputLabel>
      <Input
        id={id}
        error={error}
        {...rest}
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              aria-label="toggle password visibility"
              onClick={() => setVisible(!isVisible)}
              onMouseDown={(e) => e.preventDefault()}
            >
              {isVisible ? <Visibility /> : <VisibilityOff />}
            </IconButton>
          </InputAdornment>
        }
        type={isVisible ? "text" : "password"}
      />
      <FormHelperText
        id={`${id}-helper`}
        error={error}
      >
        {helperText}
      </FormHelperText>
    </FormControl>
  );
};

export default PasswordInput;
