import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import AccountCircle from "mdi-material-ui/AccountCircle";

import SignIn from "./containers/SignIn";

const useClasses = makeStyles((theme) =>
  createStyles({
    wrapper: {
      [theme.breakpoints.down(900)]: {
        // small screen
        backgroundColor: "#fff",
        padding: "8px",
      },
      [theme.breakpoints.up(900)]: {
        backgroundColor: "#eee",
        padding: "48px 32px 48px",
      },
      display: "flex",
      flex: 1,
    },
    container: {
      marginLeft: "auto",
      marginRight: "auto",
      maxWidth: "480px",
      width: "100%",
    },
    paper: {
      [theme.breakpoints.down(900)]: {
        // small screen
        boxShadow: "none",
      },
      padding: "24px 48px 48px",
      textAlign: "center",
      width: "100%",
    },
    footer: {
      [theme.breakpoints.down(900)]: {
        // small screen
        padding: "0 48px",
      },
      [theme.breakpoints.up(900)]: {
        padding: "16px 0",
      },
      height: "64px",
      width: "100%",
    },
    accountIcon: {
      color: "#aaa",
      fontSize: 120,
      margin: "16px 0",
    },
  }),
);

export interface Props {
}

const SignInPage: React.FC<Props> = () => {
  const classes = useClasses();

  return (
    <div className={classes.wrapper}>
      <div className={classes.container}>
        <Paper
          className={classes.paper}
        >
          <AccountCircle className={classes.accountIcon} />
          <SignIn />
        </Paper>
        <div
          className={classes.footer}
        >
          <Typography>
            Language: xxxxxx
          </Typography>
        </div>
      </div>
    </div>
  );
};

export default SignInPage;
