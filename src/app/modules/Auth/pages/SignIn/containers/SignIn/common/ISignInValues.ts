export type SignInType = "initial" | "email" | "mobile";

export interface ISignInValues {
  type: SignInType;
  input: string;
  country: string;
  username: string;
  password: string;
  form: {
    step: number;
  };
}
