import React from "react";

import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";

import ChevronDownIcon from "mdi-material-ui/ChevronDown"

import { Formik, Form, Field } from "formik";
import { useTranslation } from "react-i18next";
import PasswordInput from "../../components/PasswordInput";

import { ISignInValues } from "./common/ISignInValues";
import { resolveSignInType } from "./common/resolveSignInType";
import { setValues } from "./common/setValues";
import { validate } from "./common/validate";

const useClasses = makeStyles(() =>
  createStyles({
    inputWrapper: {
      display: "flex",
      margin: "8px 0 16px",
    },
    actionsWrapper: {
      display: "flex",
      marginTop: "8px",
    },
    country: {
      margin: "16px 16px 0 0",
    },
    username: {
      flex: 1,
    },
    inputChip: {
      margin: "16px 0",
    },
    password: {
      flex: 1,
    },
    next: {
      marginLeft: "auto",
    },
    submit: {
      width: "100%",
    },
  }),
);

export interface Props {
}

const SignInForm: React.FC<Props> = () => {
  const classes = useClasses();
  const { t } = useTranslation("auth");

  return (
    <React.Fragment>
      <Formik<ISignInValues>
        initialValues={{
          type: "initial",
          input: "",
          country: "66",
          username: "",
          password: "",
          form: {
            step: 1,
          },
        }}
        onSubmit={(values, actions) => {
          const { step } = values.form;

          if (step === 2) {
          } else{
            values = setValues(values);
            actions.setValues(values);
          }
        }}
        validate={validate}
        render={({
          values,
          errors,
          touched,
          handleChange,
          isSubmitting,
          isValid,
          setFieldValue,
        }) => (
            <Form>
              {values.form.step === 1 &&
                <React.Fragment>
                  <Typography variant="h5">
                    {t("title")}
                  </Typography>
                  <div
                    className={classes.inputWrapper}
                  >
                    {values.type === "mobile" && <Field
                      name="country"
                      render={({ field }: any) => (
                        <FormControl className={classes.country}>
                          <InputLabel>{t("country")}</InputLabel>
                          <Select
                            {...field}
                            input={<Input />}
                            autoWidth
                          >
                            <MenuItem value={66}>+66 (TH)</MenuItem>
                          </Select>
                        </FormControl>
                      )}
                    />}
                    <Field
                      name="username"
                      render={({ field }: any) => (
                        <TextField
                          {...field}
                          className={classes.username}
                          error={errors.username && touched.username && true}
                          helperText={errors.username && touched.username && t(errors.username)}
                          label={t(`username.${values.type}`)}
                          margin="normal"
                          onChange={(e) => {
                            setFieldValue("type", resolveSignInType(e.target.value));
                            console.log(values);
                            handleChange(e);
                          }}
                          required
                          type="text"
                        />
                      )}
                    />
                  </div>
                </React.Fragment>
              }
              {values.form.step === 2 &&
                <React.Fragment>
                  <Chip
                    clickable
                    className={classes.inputChip}
                    label={values.input || "tpanitte@hotmail.com"}
                    variant="outlined"
                    // onClick={() => setFieldValue("form.step", 1)}
                    // onDelete={() => setFieldValue("form.step", 1)}
                    deleteIcon={<ChevronDownIcon />}
                  />
                  <div
                    className={classes.inputWrapper}
                  >
                    <Field
                      name="password"
                      render={({ field }: any) => (
                        <PasswordInput
                          {...field}
                          className={classes.password}
                          error={errors.password && touched.password && true}
                          helperText={errors.password && touched.password && t(errors.password)}
                          label={t("password")}
                          margin="normal"
                        />
                      )}
                    />
                  </div>
                </React.Fragment>
              }
              <div
                className={classes.actionsWrapper}
              >
                <Button
                  className={values.form.step === 1 ? classes.next : classes.submit}
                  color="primary"
                  disabled={!isValid}
                  size="large"
                  type="submit"
                  variant="contained"
                >
                  {t(values.form.step === 1 ? "submit.next" : "submit.signIn")}
                </Button>
              </div>
            </Form>
          )}
      />
    </React.Fragment>
  );
};

export default SignInForm;
