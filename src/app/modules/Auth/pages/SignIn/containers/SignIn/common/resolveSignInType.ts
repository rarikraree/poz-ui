import { SignInType } from "./ISignInValues";
import { isNumbers } from "./isNumbers";

export const resolveSignInType = (value: string): SignInType => {
  if (value.length === 0) {
    return "initial";
  } else if (value.length < 3) {
    if (isNumbers(value)) {
      return "initial";
    } else {
      return "email";
    }
  } else {
    if (isNumbers(value)) {
      return "mobile";
    } else {
      return "email";
    }
  }
}
