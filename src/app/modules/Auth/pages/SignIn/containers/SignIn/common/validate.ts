import { ISignInValues } from "./ISignInValues";
import { isEmail } from "./isEmail";
import { isNumbers } from "./isNumbers";

const Errors = {
  required: "signIn.validate.required",
  invalidFormat: "signIn.validate.invalidFormat",
};

export const validate = (values: ISignInValues) => {
  const errors: any = {};
  const { username, password } = values;
  const { step } = values.form;

  if (step === 1) {
    if (username.length === 0) {
      errors.username = Errors.required;
    } else if (username.length < 3 || (!isNumbers(username) && !isEmail(username))) {
      errors.username = Errors.invalidFormat;
    }
  }

  if (step === 2) {
    if (password.length === 0) {
      errors.password = Errors.required;
    }
  }

  return errors;
};
