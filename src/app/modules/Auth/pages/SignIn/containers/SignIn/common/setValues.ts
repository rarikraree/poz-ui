import { ISignInValues } from "./ISignInValues";

export const setValues = (values: ISignInValues): ISignInValues => {
  const { step } = values.form;

  if (step === 1) {
    let username = values.username.trim();
    const type = values.type;

    if (type === "email") {
      username = username.toLowerCase();
      values.username = username
      values.input = username;
    } else {
      let offset = true;
      let numbers = "";
      const regex = /[1-9]/;

      for (const char of username) {
        if (offset) {
          if (regex.test(char)) {
            numbers += char;
            offset = false;
          }
        } else {
          numbers += char;
        }
      }

      values.username = numbers;
      values.input = `+${values.country}${numbers}`;
    }

    values.password = "";
    values.form.step = step + 1;
  }

  return values;
};
