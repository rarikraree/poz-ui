export const isNumbers = (value: string) =>
  /^\d+$/.test(value);
