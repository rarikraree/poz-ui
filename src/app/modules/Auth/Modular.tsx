import React from "react";

import SignIn from "./pages/SignIn";

interface Props {
  match: {
    path: string;
    url: string;
    isExact: boolean;
  };
}

const Modular: React.FC<Props> = ({ match }) => {
  return (
    <SignIn />
  );
};

export default Modular;
