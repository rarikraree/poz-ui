import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { composeWithDevTools } from "redux-devtools-extension";

import { AppReducers } from "./AppReducers";
import AppSagas from "./AppSagas";

const SagasMiddleware = createSagaMiddleware();

const AppStore = createStore(
  AppReducers,
  composeWithDevTools(applyMiddleware(SagasMiddleware)),
);

SagasMiddleware.run(AppSagas);

export default AppStore;
